import axios from "axios";

async function get_random_brewires(){
    let response = await axios.get("https://api.openbrewerydb.org/v1/breweries/random?size=3")
    if(response.status == 200){
        return response.data
    }
}

export default get_random_brewires
import { useState, useEffect } from 'react'
import './App.css'
import get_random_brewires from "./brewery_api"


const styles = {
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

  },
  child: {
    backgroundColor: '#808080',
    padding: '20px',
    margin: '10px',
    borderRadius: "15px"
  },
};

const buttonStyles = {
  overflow: 'hidden',
  border: '1px solid white',
  fontFamily: 'Arial',
  fontSize: '1rem',
  color: 'white',
  background: 'none',
  cursor: 'pointer',
  padding: '15px',
  display: 'inline-block',
  // margin: '15px 30px',
  textTransform: 'uppercase',
  outline: 'none',
  position: 'relative',
  transition: 'all 0.3s',
  borderRadius: '4rem',
  // fontWeight: 700,
};


function App() {
  const [breweries, setBreweries] = useState([])
  const [isLoading, setLoading] = useState(true)
  useEffect(() => {

    const getBreweries = async () => {
      let response = await get_random_brewires()
      setLoading(false)
      setBreweries(response)

    }

    getBreweries()

  }, [])

  return (
    <div>
      <h1>Random Breweries</h1>
      <div style={styles.container}>
        {
          isLoading ? <h1>Loading...</h1> :
            breweries.map((brewery) => (
              <div key={brewery.id} style={styles.child}>
                <h3>{brewery.name}</h3>
                <a style={buttonStyles} href={brewery.website_url}>website</a>
              </div>
            ))
        }
      </div>

    </div>
  )
}

export default App

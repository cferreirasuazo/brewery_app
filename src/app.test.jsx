import React from 'react';
import {render} from '@testing-library/react';
import App from './App';

describe('App test', () => {
  test('Should load App component', () => {
    render(<App />);

    expect('Random Breweries').toBeDefined();
  });
});